﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class HighScores : IComparable<HighScores>
{

    public int Score { get; set;}

    public string Name {get; set;}

    public DateTime Date{get; set;}

    public int ID { get; set; }

    public HighScores(int id, int score, string name, DateTime date)
    {
        this.Score = score;
        this.Name = name;
        this.ID = id;
        this.Date = date;
    }

    public int CompareTo(HighScores other)
    {
        if(other.Score<this.Score)
        {
            return -1;
        }
        if(other.Score>this.Score)
        {
            return 1;
        }
        else if (other.Date < this.Date)
        {
            return -1;
        }
        else if (other.Date>this.Date)
        {
            return 1;
        }
        return 0;
    }

}
